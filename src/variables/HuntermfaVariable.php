<?php
/**
 * huntermfa plugin for Craft CMS 3.x
 *
 * Specific code for the mfa205Hudson site
 *
 * @link      gregsimsic.com
 * @copyright Copyright (c) 2020 Greg Simsic
 */

namespace gregsimsic\huntermfa\variables;

use gregsimsic\huntermfa\Huntermfa;

use Craft;
use function Craft\craft;

/**
 * @author    Greg Simsic
 * @package   Huntermfa
 * @since     1.0.0
 */
class HuntermfaVariable
{
    // Public Methods
    // =========================================================================

    /**
     * @param null $optional
     * @return string
     */
    public function exampleVariable($optional = null)
    {
        $result = "And away we go to the Twig template...";
        if ($optional) {
            $result = "I'm feeling optional today...";
        }
        return $result;
    }

    public function test()
    {
        return 'test in Hudson205Variable';
    }

    public function studentIsAlumni($student)
    {
        return Huntermfa::getInstance()->huntermfa->studentIsAlumni($student);
    }

    public function getCurrentSemesterCode()
    {
        return Huntermfa::getInstance()->huntermfa->getCurrentSemesterCode();
    }

    public function columnize($string)
    {
        return Huntermfa::getInstance()->huntermfa->columnize($string);
    }

    public function getPrecedingSemesterCode()
    {
        $year = date('Y');
        $semester = date('n') < 7 ? 1 : 2;

        return $year . '-' . $semester;
    }

    public function resolveSidebarContent($entry) {

        return Huntermfa::getInstance()->huntermfa->resolveSidebarContent($entry);
    }
}
