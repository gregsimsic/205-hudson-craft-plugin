<?php
/**
 * huntermfa plugin for Craft CMS 3.x
 *
 * Specific code for the mfa205Hudson site
 *
 * @link      gregsimsic.com
 * @copyright Copyright (c) 2020 Greg Simsic
 */

namespace gregsimsic\huntermfa\fields;

use Craft\AttributeType;
use gregsimsic\huntermfa\Huntermfa;
use gregsimsic\huntermfa\assetbundles\semesterdropdownfield\SemesterDropdownFieldAsset;

use Craft;
use craft\base\ElementInterface;
use craft\base\Field;
use craft\helpers\Db;
use yii\db\Schema;
use craft\helpers\Json;
use function Craft\craft;

/**
 * @author    Greg Simsic
 * @package   Huntermfa
 * @since     1.0.0
 */
class SemesterDropdown extends Field
{
    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $someAttribute = 'Some Default';

    // Static Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public static function displayName(): string
    {
        return Craft::t('huntermfa', 'Semester Dropdown');
    }

    // Public Methods
    // =========================================================================


    /**
     * @inheritdoc
     */
    public function getContentColumnType(): string
    {
        return Schema::TYPE_STRING;
    }

    /**
     *
     * transform after coming from DB
     *
     * @inheritdoc
     */
    public function normalizeValue($value, ElementInterface $element = null)
    {
        return $value;
    }

    /**
     *
     * transform both coming from and going to the DB
     *
     * @inheritdoc
     */
    public function serializeValue($value, ElementInterface $element = null)
    {
        if (is_array($value)) {
            return $value['year'] . '-' . $value['semester'];
        }

        return $value;
    }

    /**
     * @inheritdoc
     */

    public function getInputHtml($value, ElementInterface $element = null): string
    {

        // Register our asset bundle
        Craft::$app->getView()->registerAssetBundle(SemesterDropdownFieldAsset::class);

        // Get our id and namespace
        $id = Craft::$app->getView()->formatInputId($this->handle);
        $namespacedId = Craft::$app->getView()->namespaceInputId($id);

        // If value is null, we set it to an array to prevent template errors
        if(!$value) {
            $value = Huntermfa::getInstance()->huntermfa->getCurrentSemesterCode();
        }

        $years = [];
        foreach (range( date('Y') + 5, 1980 ) as $year) {

            $years[] = array(
                'label' => $year,
                'value' => $year
            );
        }

        list($year, $semester) = explode('-', $value);

        $value = [
            'year' => $year,
            'semester' => $semester
        ];

        return Craft::$app->getView()->renderTemplate(
            'huntermfa/_components/fields/SemesterDropdown_input',
            [
                'name'  => $this->handle,
                'id' => $id,
                'namespacedId' => $namespacedId,
                'value' => $value,
                'options' => [
                    'years' => $years,
                    'semesters' => [
                        ['label' => 'Spring', 'value' => '1'],
                        ['label' => 'Fall', 'value' => '2']
                    ]
                ]
            ]
        );
    }

    protected function getOptionsSettingsLabel()
    {
        return \Craft\Craft::t('Semester Dropdown Options');
    }

}
