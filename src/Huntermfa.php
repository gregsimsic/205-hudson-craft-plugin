<?php
/**
 * huntermfa plugin for Craft CMS 3.x
 *
 * Specific code for the mfa205Hudson site
 *
 * @link      gregsimsic.com
 * @copyright Copyright (c) 2020 Greg Simsic
 */

namespace gregsimsic\huntermfa;

use gregsimsic\huntermfa\services\Huntermfa as HuntermfaService;
use gregsimsic\huntermfa\variables\HuntermfaVariable;
use gregsimsic\huntermfa\twigextensions\HuntermfaTwigExtension;
use gregsimsic\huntermfa\models\Settings;
use gregsimsic\huntermfa\fields\SemesterDropdown as SemesterDropdownField;
use gregsimsic\huntermfa\fields\SEO as SEOField;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\UrlManager;
use craft\services\Fields;
use craft\web\twig\variables\CraftVariable;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterUrlRulesEvent;

use yii\base\Event;

/**
 * Class Huntermfa
 *
 * @author    Greg Simsic
 * @package   Huntermfa
 * @since     1.0.0
 *
 * @property  HuntermfaService $huntermfa
 */
class Huntermfa extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var Huntermfa
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

//        $this->setComponents([
//            'huntermfa' => HuntermfaService::class,
//        ]);

        Craft::$app->view->registerTwigExtension(new HuntermfaTwigExtension());

        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['test'] = 'huntermfa/admin/test';
                $event->rules['updatecats'] = 'huntermfa/admin/update-categories';
            }
        );

//        Event::on(
//            UrlManager::class,
//            UrlManager::EVENT_REGISTER_CP_URL_RULES,
//            function (RegisterUrlRulesEvent $event) {
//                $event->rules['cpActionTrigger1'] = 'huntermfa/admin/do-something';
//            }
//        );

        Event::on(
            Fields::class,
            Fields::EVENT_REGISTER_FIELD_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = SemesterDropdownField::class;
                $event->types[] = SEOField::class;
            }
        );

        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('huntermfa', HuntermfaVariable::class);
            }
        );

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                }
            }
        );

        Craft::info(
            Craft::t(
                'huntermfa',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    protected function createSettingsModel()
    {
        return new Settings();
    }

    /**
     * @inheritdoc
     */
    protected function settingsHtml(): string
    {
        return Craft::$app->view->renderTemplate(
            'huntermfa/settings',
            [
                'settings' => $this->getSettings()
            ]
        );
    }
}
