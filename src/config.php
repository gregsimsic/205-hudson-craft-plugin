<?php
/**
 * huntermfa plugin for Craft CMS 3.x
 *
 * Specific code for the mfa205Hudson site
 *
 * @link      gregsimsic.com
 * @copyright Copyright (c) 2020 Greg Simsic
 */

/**
 * huntermfa config.php
 *
 * This file exists only as a template for the huntermfa settings.
 * It does nothing on its own.
 *
 * Don't edit this file, instead copy it to 'craft/config' as 'huntermfa.php'
 * and make your changes there to override default settings.
 *
 * Once copied to 'craft/config', this file will be multi-environment aware as
 * well, so you can have different settings groups for each environment, just as
 * you do for 'general.php'
 */

return [

    // This controls blah blah blah
    "someAttribute" => true,

];
