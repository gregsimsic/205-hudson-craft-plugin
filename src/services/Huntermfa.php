<?php
/**
 * huntermfa plugin for Craft CMS 3.x
 *
 * Specific code for the mfa205Hudson site
 *
 * @link      gregsimsic.com
 * @copyright Copyright (c) 2020 Greg Simsic
 */

namespace gregsimsic\huntermfa\services;

use Craft;
use craft\base\Component;

/**
 * @author    Greg Simsic
 * @package   Huntermfa
 * @since     1.0.0
 */
class Huntermfa extends Component
{
    public function getNextSemester()
    {
        return 'get next semester in Huntermfa';
    }

    public function getCurrentSemesterCode()
    {
        $year = date('Y');
        $semester = date('n') < 7 ? 1 : 2;

        return $year . '-' . $semester;
    }

    public function studentIsAlumni($student) {

        return $student->graduationSemester < $this->getCurrentSemesterCode();
    }

    public function columnize($string) {

        $break = '<!--pagebreak-->'; // the redactor pagebreak html element
        $parts = explode($break, $string);
        $columnCount = count($parts);

        if($columnCount > 1) {
            $html = "";
            $classes = "a-multiColumnBody__column";

            foreach ( $parts as $i => $part ) {
                if($i === 0) $classes .= " a-multiColumnBody__column--first";
                if($i === $columnCount - 1) $classes .= " a-multiColumnBody__column--last";
                $html .= "<div class='$classes'>" . $part . "</div>";
            }

            return $html;
        } else {
            return $string;
        }
    }

    public function resolveSidebarContent($entry) {

        // check flag
        if($entry->inheritSidebarsFromParent) {
            $parentEntrySidebarContent = $this->getParentEntrySidebarContent( $entry );
            return $parentEntrySidebarContent;
        }

        // sidebars attached to the entry
        return $entry->sidebarContent;

    }

    public function getParentEntrySidebarContent($entry) {

        function getParentSidebarContentRecursive($entry) {

            $parent = $entry->getParent();

            if ($parent) {

                if($parent->sidebarContent->count()) {
                    return $parent->sidebarContent;
                } else {
                    return getParentSidebarContentRecursive($parent);
                }

            } else {
                return false;
            }
        }

        $sidebarContent = getParentSidebarContentRecursive($entry);

        return $sidebarContent;

    }

    /*
     * This was in use on the Craft 3 site with a differenty sitemap plugin
     * it dynamically adds students to the sitemap, excluding alumni
     *
     * Add Current Students to the sitemap
     * They are excluded from the plugin in order to filter out alumni
     */
//    public function renderSitemap()
//    {
//        // Get all students
//        $criteria = craft()->elements->getCriteria(ElementType::Entry);
//        $criteria->section = 'students';
//        $criteria->limit = null;
//        $students = $criteria->find();
//
//        foreach ($students as $student)
//        {
//
//            if( !craft()->hudson205->studentIsAlumni($student)) {
//
//                $lastmod = $student->dateUpdated;
//
//                // Add the URL to the sitemap
//                craft()->sitemap->addUrl($student->url, $lastmod, Sitemap_ChangeFrequency::Daily, 0.5);
//
//            }
//
//        }
//    }
}
