<?php
/**
 * huntermfa plugin for Craft CMS 3.x
 *
 * Specific code for the mfa205Hudson site
 *
 * @link      gregsimsic.com
 * @copyright Copyright (c) 2020 Greg Simsic
 */

namespace gregsimsic\huntermfa\twigextensions;

use gregsimsic\huntermfa\Huntermfa;

use Craft;

/**
 * @author    Greg Simsic
 * @package   Huntermfa
 * @since     1.0.0
 */
class HuntermfaTwigExtension extends \Twig_Extension
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Huntermfa';
    }

    /**
     * @inheritdoc
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('someFilter', [$this, 'someInternalFunction']),
        ];
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction(
                'breakpoint',
                array($this, 'setBreakpoint'),
                array('needs_environment' => true, 'needs_context' => true)),
        );
    }

    /**
     * If XDebug is detected, makes the debugger break.
     *
     * @param Twig_Environment $environment the environment instance
     * @param mixed            $context     variables from the Twig template
     */
    public function setBreakpoint(\Twig_Environment $environment, $context)
    {
        if (function_exists('xdebug_break')) {
            $arguments = array_slice(func_get_args(), 2);
            xdebug_break();
        }
    }

    /**
     * @param null $text
     *
     * @return string
     */
    public function someInternalFunction($text = null)
    {
        $result = $text . " in the way";

        return $result;
    }
}
