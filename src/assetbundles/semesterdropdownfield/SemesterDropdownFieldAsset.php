<?php
/**
 * huntermfa plugin for Craft CMS 3.x
 *
 * Specific code for the mfa205Hudson site
 *
 * @link      gregsimsic.com
 * @copyright Copyright (c) 2020 Greg Simsic
 */

namespace gregsimsic\huntermfa\assetbundles\semesterdropdownfield;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * @author    Greg Simsic
 * @package   Huntermfa
 * @since     1.0.0
 */
class SemesterDropdownFieldAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = "@gregsimsic/huntermfa/assetbundles/semesterdropdownfield/dist";

        $this->depends = [
            CpAsset::class,
        ];

        $this->js = [
            'js/SemesterDropdown.js',
        ];

        $this->css = [
            'css/SemesterDropdown.css',
        ];

        parent::init();
    }
}
