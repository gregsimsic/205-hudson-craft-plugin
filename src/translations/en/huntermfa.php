<?php
/**
 * huntermfa plugin for Craft CMS 3.x
 *
 * Specific code for the mfa205Hudson site
 *
 * @link      gregsimsic.com
 * @copyright Copyright (c) 2020 Greg Simsic
 */

/**
 * @author    Greg Simsic
 * @package   Huntermfa
 * @since     1.0.0
 */
return [
    'huntermfa plugin loaded' => 'huntermfa plugin loaded',
];
